import { Link, NavLink } from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext"
import 'bootstrap/dist/css/bootstrap.min.css';
import AccountCircleIcon from '@material-ui/icons/AccountCircleRounded';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'


import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

	const {user} = useContext(UserContext);


	return(
		<Navbar sticky='top' bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/" >
	        <img   className=" logoImg " src={require('./logo.jpg')} />
	        </Navbar.Brand>
	   			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
  			<Navbar.Collapse id="responsive-navbar-nav">
					<Nav className="ms-auto" defaultActiveKey="/">
  				            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
  				           {
  				           	(user.isAdmin)
  				           	?
  				           	<Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>

  				           	: 
  				           	<Nav.Link as={Link} to="/products" eventKey="/courses">Products</Nav.Link>

  				           }

  				            {	
  				            	(user.id !== null)
  				            	?
  				            		<>
  				            		<Nav.Link as={Link} to="/courses" eventKey="/courses">Subjects Enrolled</Nav.Link>
  				            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
  				            		</>
  				            	:
  				            		<>
  					            		<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
  					            		<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
  				            		</>
  				            }
  				             				<Nav.Link as={Link} to='/cart'>
       					<FontAwesomeIcon icon={faShoppingCart} /> Cart
       				</Nav.Link>
    			</Nav>
  			</Navbar.Collapse>
  			</Container>
		</Navbar>
	)
}







