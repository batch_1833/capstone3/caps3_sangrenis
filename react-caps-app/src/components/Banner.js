import { Fragment } from 'react';

export default function Banner() {
    return (
        <Fragment>
            <div className="d-flex flex-column align-items-center justify-content-center text-center my-5 mx-lg-5">
                <h2>Good Shoes Take you Good Places</h2>
                <p className="mt-3 ">Shoes are not only worn to protect the human feet. They are also worn because they add the final touch to the style you are trying to create. The history of shoes is very long and nobody really knows when the first shoes were created but we do know that they were originally made to warm the feet. Now, there are many different types and each kind helps you function a different way. The shoe is also made up of many parts and every kind of shoe has different parts then other types. Year by year, shoes have been improving and becoming more and more popular. Now, we have all the equipment and all the materials we need to make the shoe better than ever.</p>

            </div>
        </Fragment>
    )
}